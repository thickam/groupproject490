import numpy as np
import matplotlib.pyplot as plt
import cv2
from copy import deepcopy


def get_bb_img(img, bb):
    x1 = bb[0]  # Left
    x2 = bb[0] + bb[2] + 1  # Right
    y1 = bb[1]  # Top
    y2 = bb[1] + bb[3] + 1  # Bottom

    return pad_to_50(img[y1:y2, x1:x2])


def display(img, bb):
    bb_img = get_bb_img(img, bb)

    plt.imshow(bb_img, cmap='gray')
    plt.show()


def pad_to_50(img):
    shape = img.shape
    while shape[0] >= 18 or shape[1] >= 18:
        img = cv2.resize(img, (0, 0), fx=.75, fy=.75)
        shape = img.shape
    w = shape[1]
    h = shape[0]

    new_img = np.zeros((50, 50), dtype='float32')
    x_offset = (50 - w) // 2
    y_offset = (50 - h) // 2

    new_img[y_offset:y_offset + h, x_offset:x_offset + w] = img
    return new_img


def sort_boxes(boxes):
    container = []

    while len(boxes) > 0:
        minbox = boxes[0]
        minscore = 601 * 1201
        for box in boxes:
            if box[1] < minbox[1]:
                minbox = deepcopy(box)
        for box in boxes:
            if box[0] < minbox[0] and box[1] in range(int(minbox[1] * .25), int(minbox[1] * 1.75)):
                minbox = deepcopy(box)
                minscore = minbox[0] * minbox[1]
        container.append(minbox)
        boxes.remove(minbox)

    return container


def split_box(box, i, avgi):
    b_c = []
    split = i + 2

    b_c.append((box[0], box[1], box[2] // split, box[3]))
    w = b_c[0][2]
    if w < 10:
        if split == 2:
            split = 1
            return [box]
        else:
            split -= 1
        while len(b_c) < split:
            b_c.append((b_c[-1][0] + b_c[-1][2], box[1], box[2] // split, box[3]))
        return b_c
    if abs((w - avgi)) / avgi > 1.05 and w > 10:
        return split_box(box, i + 1, avgi)
    else:
        while len(b_c) < split:
            b_c.append((b_c[-1][0] + b_c[-1][2], box[1], box[2] // split, box[3]))

    return b_c


def segment(file):
    img = cv2.imread(file)
    im = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    im = cv2.threshold(im, 135, 255, 0)[1]
    im = cv2.adaptiveThreshold(im, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

    imgInv = (im * -1) + 255  # Inverts the colors. Black becomes white, white becomes black.

    z = img * 0

    imgInv = np.asarray(imgInv, dtype='uint8')
    im, contours, hierarchy = cv2.findContours(imgInv, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_TC89_KCOS)

    b = []
    avg = 0
    i = 0
    for i in range(len(contours)):
        container = []
        for j in range(len(contours[i])):
            container.append(contours[i][j][0])

        container = np.asarray(container)
        box = cv2.boundingRect(container)
        if box[2] * box[3] < 50:
            continue
        avg += box[2]
        i += 1
        b.append(box)

    avgi = avg / i
    boxes = []
    for box in b:
        w = box[2]
        if w * box[3] < 50:
            continue
        if abs((w - avgi)) / avgi > 1.15 and w > 10:
            b_c = split_box(box, 0, avgi)
            for b in b_c:
                boxes.append(b)
        else:
            boxes.append(box)

    c = []
    for b1 in boxes:
        x1, y1, w1, h1 = b1[0], b1[1], b1[2], b1[3]
        flag = True
        for b2 in boxes:
            if b1 == b2:
                continue
            x2, y2, w2, h2 = b2[0], b2[1], b2[2], b2[3]
            if (x2 + w2) > (x1 + w1) and x2 < x1 and y2 < y1 and (y2 + h2) > (y1 + h1):
                flag = False
        if flag:
            c.append(b1)

    co = np.asarray(c)

    for box in co:
        x, y, w, h = box[0], box[1], box[2], box[3]

        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 0)

    boxes = sort_boxes(c)
    images = []
    for box in boxes:
        images.append(get_bb_img(imgInv, box))


    plt.imshow(img, cmap='gray')
    plt.show()

    return images[-46:]
