from django.http import JsonResponse
from django.template import Context, loader
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import base64
import uuid
import os
import GroupProject.segment as seg
import GroupProject.charScore as score


# Returns the HTML page from our template folder when the user tries to access the host URL
def index(request):
    template = loader.get_template("index.html")
    return HttpResponse(template.render())


# This is our API method that takes posts and returns a float in JSON format
# CSRF exempt this. we're going for one-shot project, not security
@csrf_exempt
def neatness(request):
    # Skip the prefix from javascript encoding
    _bytes = request.body[22:]

    # Generate a UUID so we can save the image uniquely
    _id = uuid.uuid4()

    # Generate our file name complete with the path it's going in ("GroupProject/images")
    new_file_name = os.path.join("GroupProject/images/", "image_" + str(_id) + ".png")

    # If, in the off chance the image exists already, keep generating new UUIDs until we get a unique one
    while os.path.isfile(new_file_name):
        _id = uuid.uuid4()
        new_file_name = os.path.join("GroupProject/images/", "image_" + str(_id) + ".png")

    # Open the file for writing in bytes
    with open(new_file_name, "wb") as fh:
        # Write the decoded bytes (base64 by the way) to the file and close it
        fh.write(base64.decodebytes(_bytes))
        fh.close()

    # Split the image file into individual letter images (segments)
    segments = seg.segment(new_file_name)

    # Get our neatness score
    n = score.char_classifier_predict_multi(segments)

    # Return our JSON response with our neatness value!
    return JsonResponse({'neatness': n})
