// JQuery shorthand for window.onload (so we know everything we need has been added to the dom)
$(function () {
    $('#qrcode').qrcode(window.location.href);
});

// Initialize our
var recentImages = [];

// Maximum kept recent images
var maxAllowedRecentImages = 5;

/***
 * Toggles the hidden property of the qr code container element
 */
function toggleQRCode() {
    getQRCodeElement().hidden = !getQRCodeElement().hidden;
}

/***
 * Pushes an imageURL along with its neatness score and the image name to the recentImages array.
 * @param imageURL - Data URL of the image
 * @param imageName - Name of the file that was uploaded - don't have to be unique
 * @param neatnessScore - Floating point value between 0 and 1 representing the neatness of the image (1 being neatest)
 */
function pushRecentImage(imageURL, imageName, neatnessScore) {
    recentImages.push({
        url: imageURL,
        fileName: imageName,
        neatnessScore: neatnessScore
    });
    // If we have more than our max allowed
    if (recentImages.length > maxAllowedRecentImages) {
        // Array.shift() is Array.pop() from the opposite direction - remove first element
        recentImages.shift();
    }
}

/***
 * Moves elements in an array based on a from and to index
 * @param array - Array to move elements of
 * @param from - The index at which the element we are moving exists
 * @param to - The index we are wanting to move that element to
 * @returns {Array} - The resulting array
 */
function moveArrayElement(array, from, to) {
    // If we are moving and element to its current position, just return the current array
    if (to === from) return array;

    // Get our target array element
    var target = array[from];

    // get the direction we need to go
    var increment = to < from ? -1 : 1;

    // Shift all other elements according to the increment sign
    for (var k = from; k !== to; k += increment) {
        array[k] = array[k + increment];
    }

    // Assign the target to its destination index
    array[to] = target;

    // Return our new array
    return array;
}

/***
 * Returns the <ul> Element we want to append image <li>s to
 * @returns {Node}
 */
function getRecentListElement() {
    return $('#recentList')[0];
}

/***
 * Returns the <div id="qrcode"/> element that displays a QR code pointing to the host
 * @returns {Node}
 */
function getQRCodeElement() {
    return $('#qrcode')[0];
}

/***
 * Returns the <input type="file"> Element that users upload images to
 * @returns {Node}
 */
function getInputElement() {
    return $('#inputFileToLoad')[0];
}

/***
 * Returns the <img> element that displays in the middle of the page
 * @returns {Node}
 */
function getImageElement() {
    return $('#image')[0];
}

/***
 * Returns the <figcaption> element that shows the current image's name
 * @returns {Node}
 */
function getCaptionElement() {
    return $('#caption')[0];
}

/***
 * Returns the <div> element holding the ^ that acts as our pointer to the gradient
 * @returns {Node}
 */
function getGradientPointerElement() {
    return $('#gradient-pointer')[0];
}

/***
 * Returns the <h1> element that displays the neatness number
 * @returns {Node}
 */
function getNeatnessElement() {
    return $('#neatness')[0];
}

/***
 * Returns the <div> element that displays the error message
 * @returns {Node}
 */
function getErrorElement() {
    return $('#error-msg')[0];
}

/***
 * Returns the <spand> element that displays the error message text
 * @returns {Node}
 */
function getErrorTextElement() {
    return $('#error-msg-txt')[0];
}

/***
 * Returns the <div> element that acts as the spinner container
 * @returns {Node}
 */
function getSpinnerElement() {
    return $('#spinner')[0];
}

/***
 * Returns the <div> element that wraps the whole neatness display (neatness title, gradient line/pointer, neatness #
 * @returns {Node}
 */
function getNeatnessContainerElement() {
    return $('#neatness-container')[0];
}

/***
 * Changes the text of the image caption to the passed in text
 * @param newText {String} - Text to be displayed under the image
 */
function changeCaptionText(newText) {
    getCaptionElement().innerText = newText;
}

/***
 * onClick event handler for recent images, moves the image to the end of the array (top of recent) and displays it
 * @param event
 */
function selectRecentImage(event) {
    // Initialize our variable to an empty string
    var imageUrl = "";
    // The event is the recent image container, its children are an img element and the name of the image
    // For each child
    event.currentTarget.childNodes.forEach(function (child) {
        // If it's got a src attribute, it's the img and we want to grab that attribute's value
        if (child.hasAttribute("src")) {
            imageUrl = child.getAttribute("src");
            // Set the main image node's source url to this one
            getImageElement().src = imageUrl;
            // Otherwise, it's the name element
        } else {
            // Change the main image caption to this one
            changeCaptionText(child.innerText);
        }
    });

    // Move the array element that we clicked to the most recent
    recentImages = moveArrayElement(recentImages, indexOfUrl(imageUrl), recentImages.length - 1);

    // Re-render the list of recent images
    updateRecentList();

    // Update the neatness display with our current image
    adjustNeatnessDisplay(recentImages[recentImages.length - 1].neatnessScore);
}

/***
 * Renders an individual recent image element from the recentImage object inside the recentImages collection
 * @param recentImage - An element of the recentImages array (or another object of that shape)
 * @returns {HTMLLIElement} - The <li> element to be inserted into the recent images <ul>
 */
function renderRecentImage(recentImage) {
    // Make a div, set inner text to the file name, add the 'text-truncate' css class
    var nameContainer = document.createElement("div");
    nameContainer.innerText = recentImage.fileName;
    nameContainer.classList.add("text-truncate");

    // Create an image element and set the source property to the image's data url. Add the 'recent-image' css class
    var imgElement = document.createElement("img");
    imgElement.classList.add("recent-image");
    imgElement.src = recentImage.url;

    // Create the anchor element, add the css class 'nav-link', set href to '#' so it doesn't navigate
    var anchorElement = document.createElement("a");
    anchorElement.classList.add("nav-link");
    anchorElement.href = "#";
    // Don't forget to set the onclick method here
    anchorElement.onclick = selectRecentImage;

    // Put the <img> and <div> elements inside the anchor tag
    anchorElement.appendChild(imgElement);
    anchorElement.appendChild(nameContainer);

    // Create an <li> element and add the 'nav-item' css class
    var listItem = document.createElement("li");
    listItem.classList.add("nav-item");

    // Add the anchor element to the <li> and return the <li>
    listItem.appendChild(anchorElement);
    return listItem;
}

/***
 * Removes all children from a DOM Node (used to empty the <ul> so we can just re-render them all at once)
 * @param element
 */
function removeAllChildren(element) {
    // While there is a first child
    while (element.firstChild) {
        // Remove it
        element.removeChild(element.firstChild);
    }
}

/***
 * Updates the recent list DOM to reflect the current recentImages array
 */
function updateRecentList() {
    // Grab a reference to the <ul> element and remove all children
    var listElement = getRecentListElement();
    removeAllChildren(listElement);

    // For each recentImages element
    for (var i = recentImages.length - 1; i >= 0; --i) {
        // Create and append the proper recent image element
        listElement.appendChild(renderRecentImage(recentImages[i]));
    }
}

/***
 * Returns the index of the recentImages element that matches the passed in data url
 * @param url - The data url of an image you expect to find in the recentImages array
 * @returns {number} - Returns the index of the element containing the passed-in URL or -1
 */
function indexOfUrl(url) {
    // Array.find returns the element that makes its anonymous function return true
    var ele = recentImages.find(function (recentImage) {
        // Only return true if the urls match
        return recentImage.url === url;
    });

    // Return the index using indexOf so returning -1 is already taken care of
    return recentImages.indexOf(ele);
}

/***
 * Processes a floating point number to be an integer with a % at the end and moving the ^ symbol
 * @param neatnessScore {float} - Floating point number (between 0 and 1) to be displayed as a percent below an image
 */
function adjustNeatnessDisplay(neatnessScore) {
    // This is just converting a float between 0 and 1 to a percent value
    var neatnessPercent = Math.round((neatnessScore * 100));
    // The position uses -50 to 50, so just subtract 50 and convert to a string (base-10 radix) with a % on the end
    var newLeftValue = (neatnessPercent - 50).toString(10) + "%";
    // Convert to string with % on the end
    var displayValue = neatnessPercent.toString(10) + "%";

    // Get the gradient pointer element and set its style's 'left' property to our newLeftValue variable
    getGradientPointerElement().style.left = newLeftValue;

    // Set the inner text to our displayValue variable
    getNeatnessElement().innerText = displayValue;

    // If we're adjusting the neatness display, we almost definitely want to show it
    setSpinner(false);
}

// Sets the visibility of the spinner, and makes the neatness display the opposite
function setSpinner(shown, errorMsg) {
    // Get our elements
    var neatnessContainer = getNeatnessContainerElement();
    var spinner = getSpinnerElement();
    var errorContainer = getErrorElement();

    // Set spinner.hidden to the opposite of shown
    spinner.hidden = !shown;

    if (errorMsg) {
        errorContainer.hidden = shown;
        neatnessContainer.hidden = true;

        var errorTextContainer = getErrorTextElement();
        errorTextContainer.innerText = errorMsg;
    } else {
        // Set the neatness container to be hidden if the spinner is shown and vice-versa
        errorContainer.hidden = true;
        neatnessContainer.hidden = shown;
    }
}

// Processes an image when the input changes
function processImage() {
    // Grab the file input element
    var input = getInputElement();

    // If there isn't a file, do nothing
    if (!input.files[0]) {
        return;
    }

    // Otherwise, get the file path and strip it of everything but the end (the file name)
    var filePathName = input.value;
    var fileName = filePathName.replace(/^.*[\\\/]/, '');

    // Go ahead and set the caption to be the image name
    changeCaptionText(fileName);

    // Get our image element and initialize a file reader object
    var imageElement = getImageElement();
    var reader = new FileReader();

    // Set the onloadend function BEFORE we try to load something
    reader.onloadend = function () {
        // The url representation of the image is in the reader once loadend is called
        var imageUrl = reader.result;
        // Go ahead and set our image element to be this so it can be displayed while we retrieve the neatness score
        imageElement.src = imageUrl;

        // Get the index of this URL from the recentImages array
        var index = indexOfUrl(imageUrl);
        if (index > -1) {
            // If this image is in the recentImages array, just act like we clicked it
            recentImages = moveArrayElement(recentImages, index, recentImages.length - 1);
            updateRecentList();
            adjustNeatnessDisplay(recentImages[recentImages.length - 1]);
        } else {
            // Otherwise, we need to get the neatness data from the server
            setSpinner(true);

            // Begin a new XMLHttpRequest
            var httpPost = new XMLHttpRequest();
            // This is the path to our server API method
            var path = "/neatness";
            // our data will just be a string containing the image data URL
            var data = imageUrl;

            // Set the statechange listener to do what we want
            httpPost.onreadystatechange = function (err) {
                // If the readyState is 4 (done) and status is 200 (ok)
                if (httpPost.readyState === 4 && httpPost.status === 200) {
                    // Get the response text, it should be a json string
                    var responseText = httpPost.responseText;
                    // Parse this json and get the 'neatness' property from it (expected from the API method)
                    var responseValue = JSON.parse(responseText).neatness;
                    // Push our recent image with our now-known responseValue (neatness score)
                    pushRecentImage(reader.result, fileName, responseValue);
                    // Adjust the neatness display
                    adjustNeatnessDisplay(responseValue);
                    // Toggle off the spinner (showing the neatness display)
                    setSpinner(false);
                    // Update the recent list to reflect our new image
                    updateRecentList();
                } else if (httpPost.readyState === 4) {
                    setSpinner(false, "An error occurred, please try again later.");
                    // If this gets hit (response 'done' but not 200 status), then there was an error so print it out
                    console.error(err);
                }
            };
            // Set the content type of the request to json since that's what's being sent
            httpPost.open("POST", path, true);
            // Set the request header to just be plain old text
            httpPost.setRequestHeader('Content-Type', 'text/plain');
            // Send that request!
            httpPost.send(data);
        }

    };

    // Grab the file
    var file = input.files[0];

    // If there is a file there (should be)
    if (file) {
        // Read the file as a data url
        reader.readAsDataURL(file);
    } else {
        imageElement.src = "";
    }
}