# import libraries
import numpy as np
from keras.models import load_model
from keras.backend import clear_session
import matplotlib.pyplot as plt


def char_classifier_predict_multi(_image_array_orig):
    # Make a copy
    _image_array = _image_array_orig

    # Convert list to nparray
    _image_array = np.asarray(_image_array)
    # Reshape array to be Nx50x50
    _image_array = _image_array.reshape(-1, 50, 50, 1)
    # Divide the values by 255
    _image_array /= 255
    # Round up to 1 or down to 0
    _image_array = np.around(_image_array, decimals=0)

    clear_session()

    # Load model previously saved
    _model = load_model('GroupProject/charClassifier.h5')

    # Make prediction on the classes
    prediction = _model.predict_classes(_image_array)

    # Assign prediction letter
    d = 'abcdefghijklmnopqrstuvwxyz'

    predicted_letters = []

    # For each prediction, push its corresponding letter
    for pred in range(len(prediction)):
        img = np.reshape(_image_array[pred], (50, 50))
        plt.imshow(img, cmap='gray')
        plt.show()
        guess = d[prediction[pred]]
        predicted_letters.append(guess)

    print(predicted_letters)

    # What really matters is how confident we are. This is the predict function
    prediction_prob = _model.predict(_image_array)

    # Initialize a score value
    score = 0
    # For each probability
    for i in prediction_prob:
        # Find the most likely prediction
        score += (np.amax(i))

    # Average out the scores
    score = score / len(_image_array)
    return score
