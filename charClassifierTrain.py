import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras import optimizers

# import data
data=pd.read_csv('A_ZHandwrittenData.csv',header=None,delimiter=',')

# y is the first pixel in the image
X=data.iloc[:,0:784]
y=data.iloc[:,0]

# split train test
from sklearn.model_selection import train_test_split

X_train,X_test,y_train,y_test=train_test_split(X,y,test_size = 0.3)

# from pandas.DataFrame import as_matrix
X_train=pd.DataFrame.as_matrix(X_train)
X_test=pd.DataFrame.as_matrix(X_test)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')

X_train = X_train.reshape(-1,28,28,1)
X_test = X_test.reshape(-1,28,28,1)
X_train /= 255
X_test /= 255

# pad training images up to 50x50
npad=((0,0),(11,11),(11,11),(0,0))
X_train_pad=np.pad(X_train,pad_width=npad,mode='constant',constant_values=0)

# pad test images up to 50x50
npad=((0,0),(11,11),(11,11),(0,0))
X_test_pad=np.pad(X_test,pad_width=npad,mode='constant',constant_values=0)

# one hot encode labels for softmax classification
y_train = np_utils.to_categorical(y_train, 26)
y_test = np_utils.to_categorical(y_test, 26)

# conv and pooling layers
model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
activation='relu',
input_shape=(50,50,1)))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

# dense layers for softmax classification of 26 classes
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.7))
model.add(Dense(26, activation='softmax'))

# compile the model
model.compile(loss='categorical_crossentropy', 
              optimizer='adadelta',
              metrics=['accuracy'])

# fit model
model.fit(X_train_pad, y_train,
          batch_size=1000, epochs=10,verbose=1,
          validation_data=(X_test_pad, y_test)

# make predictions
y_test_pred = model.predict_classes(X_test_pad)

# array of probabilities
y_test_prob=model.predict(X_test_pad)


