# README #

### What is this repository for? ###

This is the group project submission for IUPUI CSCI 49000 Introduction to Data Science

### How do I get set up? ###

* Download the zip file and unzip

* This was developed using Python 3.6.4 and pip version 10.0.0.1

* Required Packages:
    * Django
    * Numpy
    * Matplotlib
    * OpenCV-Python
    * Numpy
    * Keras
    * Pandas

* Navigate to the directory you unzipped to and run "manage.py runserver"

* If you would like to run at a specified URL or port you can add that after runserver option

Example:


```python manage.py runserver```


```python manage.py runserver 10.0.1.1:8000```

NOTE: BELOW IS UNSAFE AND SHOULD NOT BE USED UNLESS YOU KNOW WHAT YOU ARE DOING OR JUST DON'T CARE

If on windows and wanting to access server from other devices, you can use (quotes included)


```python manage.py runserver "$((Test-Connection -ComputerName (hostname) -Count 1  | Select -ExpandProperty IPV4Address).IPAddressToString):8000"```


If that doesn't work or you wish to run the project without Django, you can simply call isolation.py followed by the path to an image to classify.

```python isolation.py ./path/to/image.png```

The file used to create the classification model can be found in charClassifierTrain.py

### Developers: ###
	
Cullen Hauser

Timothy Hickam

Dave Owens

Jack Waugh